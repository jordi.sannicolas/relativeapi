package relative.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mati
 */
public class RelativeServletXml extends HttpServlet {

    private static final File DATA_FOLDER = new File("C:\\Users\\Alumne\\Desktop\\Relative\\Relative Servlet\\resources");

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        
        if (action != null) {

            switch (action) {
                case "scenarioByName":
                    requestScenarioByName(request, response);
                    break;
                default:
                    redirectError(request, response, "method_not_allowed");
                    break;
            }
        } else {

            redirectError(request, response, "no_action");

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String action = request.getParameter("action");

        if (action != null) {

            switch (action) {
                case "saveScenario":
                    saveScenario(request, response);
                    break;
                default:
                    redirectError(request, response, "method_not_allowed");
                    break;
            }

        } else {
            redirectError(request, response, "no_action");
        }

    }

    /**
     * Serves error responses.
     *
     * @param request servlet request
     * @param response servlet response
     * @param error error exception
     * @throws javax.servlet.ServletException servlet exception
     * @throws java.io.IOException ioexeption
     */
    public void redirectError(HttpServletRequest request, HttpServletResponse response, String error)
            throws ServletException, IOException {
        String errorMsg;
        int errorCode;
        switch (error) {
            case "bad_parameter": // bad parameter action
                errorMsg = "Invalid action parameter";
                errorCode = HttpServletResponse.SC_BAD_REQUEST;
                break;
            case "method_not_allowed": //method not allowed.
                errorMsg = "Method not allowed";
                errorCode = HttpServletResponse.SC_METHOD_NOT_ALLOWED;
                break;
            case "scenario_not_exist": //method not allowed.
                errorMsg = "Scenario no exist";
                errorCode = HttpServletResponse.SC_NOT_ACCEPTABLE;
                break;
            case "scenario_duplicate": //method not allowed.
                errorMsg = "Scenario duplicate, please enter another name";
                errorCode = HttpServletResponse.SC_NOT_ACCEPTABLE;
                break;
            default: // need parameter action
                errorMsg = "Parameter action needed";
                errorCode = HttpServletResponse.SC_BAD_REQUEST;
                break;
        }
        response.sendError(errorCode, errorMsg);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Project by Ander, Jordi and Michael.";
    }// </editor-fold>

    /**
     * Request scenario by name
     *
     * @param request servlet request
     * @param response servlet response
     */
    private void requestScenarioByName(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String name = request.getParameter("name");

        if (name != null) {

            requestScenario(request, response, name);

        } else {
            redirectError(request, response, "bad_parameter");
        }

    }

    /**
     * Return for http request.
     *
     * @param name Scenario name
     * @param response servlet response
     * @throws IOException writing mistake
     */
    private void requestScenario(HttpServletRequest request, HttpServletResponse response, String name)
            throws IOException, ServletException {

        String xmlNameExt = name + ".xml";

        File xml = new File(DATA_FOLDER, xmlNameExt);

        if (xml.exists()) {

            OutputStream os = response.getOutputStream();
            byte[] buffer = new byte[4 * 1024];
            int readen;
            try (FileInputStream fis = new FileInputStream(xml)) {

                while ((readen = fis.read(buffer)) != -1) {
                    os.write(buffer, 0, readen);
                }
            }

        } else {
            redirectError(request, response, "scenario_not_exist");
        }

    }

    /**
     * Save scenario into resource. Xml style.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException servlet exception
     * @throws IOException input output exception
     */
    private void saveScenario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");

        if (name == null) {

            redirectError(request, response, "bad_parameter");

        } else {

            File f = new File(DATA_FOLDER, name + ".xml");

            if (f.exists()) {

                redirectError(request, response, "scenario_duplicate");

            } else {

                String xml = request.getParameter("scenario");

                if (xml == null) {

                    redirectError(request, response, "bad_parameter");

                } else {
                    try (FileWriter fw = new FileWriter(f)) {

                        fw.write(xml);

                    } catch (Exception Ex) {
                        
                    }
                }
            }
        }
    }

}
