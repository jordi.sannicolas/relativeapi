﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Relative.Presentation.Module.Interpreter.ReosurceClasses
{
    unsafe class WrappedBitmap
    {
        #region Attributes

        private readonly Bitmap originalBitmap;
        private BitmapData data;
        private readonly Rectangle rectangle;

        private int width, height;
        int[] test;

        #endregion Attributes

        #region Contructors

        /// <summary>
        /// Constructor where init scenario
        /// </summary>
        /// <param name="bitmap">Bitmap where scenario is drawn</param>
        public WrappedBitmap(Bitmap bitmap)
        {
            originalBitmap = bitmap;

            width = bitmap.Width;
            height = bitmap.Height;

            rectangle = new Rectangle(0, 0, width, height);
            test = new int[width * height];
        }
        #endregion Contructors

        #region Methods

        /// <summary>
        /// Block internal bitmap data
        /// </summary>
        public void Lock()
        {
            data = originalBitmap.LockBits(rectangle, ImageLockMode.ReadWrite, PixelFormat.Format32bppRgb);
        }

        /// <summary>
        /// Unlocks internal bitmap data
        /// </summary>
        /// <returns>Returns a copy of the original bitmap</returns>
        public Bitmap Unlock()
        {
            originalBitmap.UnlockBits(data);
            return originalBitmap.Clone(rectangle, originalBitmap.PixelFormat);
        }

        /// <summary>
        /// Dumps the colours of a matrix directly onto the bitmap
        /// </summary>
        /// <param name="colors">Matrix of colors</param>
        public unsafe void SetPixels(int[,] colors)
        {
            System.GC.Collect();

            int* start = (int*)data.Scan0;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    *(start + y * width + x) = colors[x, height - y - 1];
                }
            }
        }

        #endregion Methods

    }
}
