﻿using Relative.Core.Api;
using Relative.Presentation.Module.Interpreter.ReosurceClasses;
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Relative.Presentation.Module.Interpreter.AccesClasses
{
    public class Interpreter
    {
        #region Attributes

        private static Interpreter instance = null;

        private const int WIDTH = 1920 / 2;
        private const int HEIGHT = 1080 / 2;

        private BitmapFPSEvent eventHandler;
        private WrappedBitmap wrappedBitmap;
        private CoreApi api;

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Singleton constructor
        /// </summary>
        /// <param name="virtualScreenPortray">Screen where where the figures are displayed</param>
        /// <param name="fpsCounter">Label where print fps update</param>
        /// <returns></returns>
        public static Interpreter GetInstance(PictureBox virtualScreenPortray, Label fpsCounter)
        {
            if (instance == null)
            {
                instance = new Interpreter(virtualScreenPortray, fpsCounter);
            }

            return instance;

        }

        /// <summary>
        /// Singleton constructor
        /// </summary>
        /// <param name="virtualScreenPortray">Screen where where the figures are displayed</param>
        /// <returns></returns>
        public static Interpreter GetInstance(PictureBox virtualScreenPortray)
        {
            if (instance == null)
            {
                instance = new Interpreter(virtualScreenPortray);
            }
            return instance;
        }

        /// <summary>
        /// Singleton constructor
        /// </summary>
        /// <param name="bitmapEvent"></param>
        /// <returns></returns>
        public static Interpreter GetInstance(BitmapEvent bitmapEvent)
        {
            if (instance == null)
            {
                instance = new Interpreter(bitmapEvent);
            }
            return instance;
        }

        public static Interpreter GetInstance(BitmapFPSEvent eventHandler)
        {
            if (instance == null)
            {
                instance = new Interpreter(eventHandler);
            }
            return instance;
        }

        private Interpreter()
        {
            api = CoreApi.GetInstance(WIDTH, HEIGHT);
        }

        private Interpreter(PictureBox virtualScreenPortray, Label fpsCounter) : this()
        {
            eventHandler = delegate (Bitmap bitMap, int fps)
            {
                virtualScreenPortray.Image = bitMap;
                fpsCounter.Text = fps.ToString();
            };
        }

        private Interpreter(PictureBox pictureBox) : this()
        {
            eventHandler = delegate (Bitmap bitMap, int fps)
            {
                pictureBox.Image = bitMap;
            };
        }

        private Interpreter(BitmapEvent bitmapEvent) : this()
        {
            eventHandler = delegate (Bitmap bitMap, int fps)
            {
                bitmapEvent(bitMap);
            };
        }

        private Interpreter(BitmapFPSEvent eventHandler) : this()
        {
            this.eventHandler = eventHandler;
        }

        #endregion Constructors

        #region Methods

        public void Start()
        {

            this.wrappedBitmap = new WrappedBitmap(new Bitmap(WIDTH, HEIGHT));

            Update();

        }


        private async void Update()
        {

            long start;
            int frames = 0;
            Bitmap bitmap;
            while (true)
            {
                start = DateTimeOffset.Now.ToUnixTimeMilliseconds();

                Task<Bitmap> task = GeneraBitmap();

                bitmap = await task;

                frames = GetFPS(start, DateTimeOffset.Now.ToUnixTimeMilliseconds());

                eventHandler(bitmap, frames);

            }
        }

        public Task<Bitmap> GeneraBitmap()
        {

            return Task.Run(Render);
        }

        private Bitmap Render()
        {
            api.GenerateMatrix();

            wrappedBitmap.Lock();

            wrappedBitmap.SetPixels(api.GetMatrix());

            return (Bitmap)wrappedBitmap.Unlock().Clone();
        }

        public int GetFPS(long start, long end)
        {
            return (int)(1000 / Math.Max(1, end - start));
        }

        #endregion Methods

        public delegate void BitmapEvent(Bitmap bitmap);

        public delegate void BitmapFPSEvent(Bitmap bitmap, int fps);

    }
}
