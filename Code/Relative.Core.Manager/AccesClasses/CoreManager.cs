﻿using Relative.Core.Manager.ResourceClasses;
using Relative.Core.Manager.ResourceClasses.Model;
using System.Collections.Generic;

namespace Relative.Core.Manager
{
    public class CoreManager
    {
        #region Attributes

        private static CoreManager instance = null;
        private Generator generator;
        private Scenario scenario;

        #endregion Attributes

        #region Constructors

        private CoreManager(int w, int h)
        {
            generator = new Generator(w, h);
        }

        private CoreManager() { }

        /// <summary>
        /// Singleton constructor for interpreter through api
        /// </summary>
        /// <param name="w">VirtualScreen width</param>
        /// <param name="h">VirtualScreen height</param>
        /// <returns>Manager acces</returns>
        public static CoreManager GetInstance(int w, int h)
        {
            if (instance == null)
            {
                instance = new CoreManager(w, h);
            }
            else
            {
                instance.generator = new Generator(w, h);
                instance.Width = w;
                instance.Height = h;
            }

            return instance;
        }

        /// <summary>
        /// Singleton constructor for transformator through api
        /// </summary>
        /// <param name="s">Collection of figures</param>
        /// <returns>Manager acces</returns>
        public static CoreManager GetInstance(Scenario s)
        {
            if (instance == null)
            {
                instance = new CoreManager();
                instance.scenario = s;
            }

            return instance;
        }

        #endregion Constructors

        #region Getters and Setters

        public int Width { get; set; }
        public int Height { get; set; }

        public int[,] GetMatrix()
        {
            return generator.Matrix;
        }

        public Scenario GetScenario()
        {
            return scenario;
        }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Method used to start manager tasks
        /// </summary>
        public void Run()
        {
            GenerateMatrix();
        }

        /// <summary>
        /// Method used to inizializate VirtualScreen (Matrix)
        /// </summary>
        private void GenerateMatrix()
        {
            List<Figure> transformedFigures = TransformFigures();
            List<Figure> projectedFigures = ProjectFigures(transformedFigures, scenario.Camera, scenario.Fov);

            generator.GenerateMatrix(scenario, projectedFigures);
        }

        /// <summary>
        /// Method used to apply transformations to figure (Rotate, scale)
        /// </summary>
        /// <returns>Transformed figures</returns>
        public List<Figure> TransformFigures()
        {
            List<Figure> transformed = new List<Figure>();

            lock (scenario.Figures)
            {
                for (int i = 0; i < scenario.Figures.Count; i++)
                {
                    transformed.Add(Transformation.TransformateFigure(scenario.Figures[i]));
                }
            }

            return transformed;
        }

        /// <summary>
        /// Method used to apply FOV to scenario
        /// </summary>
        /// <param name="figures">Collection of figures</param>
        /// <param name="camera">Camera to calculate FOV from</param>
        /// <returns></returns>
        public List<Figure> ProjectFigures(List<Figure> figures, Vertex camera, double fov)
        {
            List<Figure> projected = new List<Figure>();

            for (int i = 0; i < figures.Count; i++)
            {
                projected.Add(Fov.ProjectFigure(figures[i], Width, camera, fov));
            }

            return projected;
        }

        #endregion Methods

    }
}
