﻿using Relative.Core.Manager.ResourceClasses.Model;
using System;
using System.Collections.Generic;

namespace Relative.Core.Manager
{
    class Generator
    {
        #region Attributes

        private int width, height;

        #endregion Attributes

        #region Constructor

        public Generator(int w, int h)
        {
            width = w;
            height = h;
        }

        #endregion Constructor

        #region Getters and Setters

        public int[,] Matrix { private set; get; }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Method used to fill Matrix
        /// </summary>
        /// <param name="scenario">Scenario data</param>
        /// <param name="transformedFigures">Figures to print</param>
        public void GenerateMatrix(Scenario scenario, List<Figure> transformedFigures)
        {
            Matrix = new int[width, height];

            if (scenario.Figures != null)
            {
                lock (scenario.Figures)
                {
                    PrintFigures(transformedFigures);
                }
            }

        }

        /// <summary>
        /// Method used to print figures
        /// </summary>
        /// <param name="transformedFigures">Figures with Rotation and Scale applied</param>
        private void PrintFigures(List<Figure> transformedFigures)
        {
            for (int i = 0; i < transformedFigures.Count; i++)
            {
                if (transformedFigures[i].Faces != null)
                {
                    PrintFaces(transformedFigures[i]);
                }

            }
        }

        /// <summary>
        /// Method used to print face
        /// </summary>
        /// <param name="figure">Figure which face belong to</param>
        private void PrintFaces(Figure figure)
        {
            for (int i = 0; i < figure.Faces.Count; i++)
            {
                PrintSides(figure.Faces[i], figure.PositionalPoint);
            }
        }

        /// <summary>
        /// Method used to print sides
        /// </summary>
        /// <param name="face">Face where sides belong to</param>
        /// <param name="positionalPoint">Figure PositionalPoint</param>
        public void PrintSides(Face face, Vertex positionalPoint)
        {
            for (int i = 0; i < face.TransformedSides.Length; i++)
            {
                PrintSide(face.TransformedSides[i], positionalPoint);
            }
        }

        /// <summary>
        /// Method used to print one single side
        /// </summary>
        /// <param name="s">Side to print</param>
        /// <param name="positionalPoint">Figure positional point</param>
        public void PrintSide(Side s, Vertex positionalPoint)
        {
            int dX = (width / 2);// - camera.X;
            int dY = (height / 2);// - camera.Y;


            if (s.multiplicator == float.PositiveInfinity)
            {
                int minY = Math.Max(0, dY + positionalPoint.Y + Math.Min(s.v1.Y, s.v2.Y));
                int maxY = Math.Min(height, dY + positionalPoint.Y + Math.Max(s.v1.Y, s.v2.Y));

                int x = positionalPoint.X + s.v1.X + dX;

                try
                {
                    if(x>=0 && x < width)
                    {
                        for (int y = minY; y < maxY; y++)
                        {
                            Matrix[x, y] |= s.Color;
                        }
                    }
                }
                catch (IndexOutOfRangeException) { }
            }
            else
            {
                int d1x = positionalPoint.X;
                int d1y = positionalPoint.Y;

                if (s.v1.X < s.v2.X)
                {
                    d1x += s.v1.X;
                    d1y += s.v1.Y;
                }
                else
                {
                    d1x += s.v2.X;
                    d1y += s.v2.Y;
                }

                d1x += dX;
                d1y += dY;

                PrintY(s, d1x, d1y);

            }

        }

        /// <summary>
        /// Method used to print pixel in Matrix Y axis previously calculated
        /// </summary>
        /// <param name="s">Side to print from</param>
        /// <param name="d1x">Minimal X point to start to print</param>
        /// <param name="d1y">Minimal Y point to start to print</param>
        public void PrintY(Side s, int d1x, int d1y)
        {
            int max = Math.Min(d1x + Math.Abs(s.v1.X - s.v2.X), width - 1);

            int oldY = d1y;
            int nextY;
            int endY;

            int cycle = 0;
            for (int x = d1x; x <= max; x++)
            {
                if (x < 0) continue;
                try
                {
                    nextY = d1y + (int)(s.multiplicator * cycle++);
                    endY = Math.Max(oldY, nextY);
                    if (nextY >= 0 && nextY < height)Matrix[x, nextY] |= s.Color;
                    else
                    {
                        if (s.multiplicator > 0)
                        {
                            if (nextY >= height) break;
                        }
                        else if (nextY < 0) break;
                    }

                    // Iterate in Y axis between two X values to fill empty pixels
                    for (int y = Math.Min(oldY, nextY); y < endY; y++)
                    {
                        if (y >= 0 && y < height) Matrix[x - 1, y] |= s.Color;
                    }
                    oldY = nextY;
                }
                catch (IndexOutOfRangeException) { }
            }
        }

        #endregion Methods

    }
}
