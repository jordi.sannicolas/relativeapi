﻿using Relative.Core.Manager.ResourceClasses.Model;
using System;

namespace Relative.Core.Manager.ResourceClasses
{
    public class Fov
    {
        /// <summary>
        /// Method used to apply FOV to figure
        /// </summary>
        /// <param name="f">Figure to project</param>
        /// <param name="wVirtualScreen">VirtualScreen width</param>
        /// <param name="camera">Camera tu calculate FOV from</param>
        /// <returns>Projected figure</returns>
        public static Figure ProjectFigure(Figure f, int wVirtualScreen, Vertex camera, double fov)
        {
            Figure res = f.Clone();

            double vScreenPos = (wVirtualScreen) / Math.Tan(fov / 2);

            Vertex v1, v2;

            foreach (Face face in res.Faces)
            {
                foreach (Side side in face.TransformedSides)
                {
                    try
                    {
                        v1 = CalculateDistanceVertex(side.v1, res.PositionalPoint, camera, vScreenPos);
                        v2 = CalculateDistanceVertex(side.v2, res.PositionalPoint, camera, vScreenPos);
                        side.SetVertices(v1, v2);
                    }
                    catch (Exception)
                    {
                        side.v1 = new Vertex(0, 0, 0);
                        side.v2 = new Vertex(0, 0, 0);
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// Method used to calculate VirtualScreen projection position
        /// </summary>
        /// <param name="v">Vertex to project</param>
        /// <param name="pp">PositionalPoint of figure which vertex belong</param>
        /// <param name="camera">Camera position</param>
        /// <param name="vScreenPos">Projection position</param>
        /// <returns>VirtualScreen position</returns>
        private static Vertex CalculateDistanceVertex(Vertex v, Vertex pp, Vertex camera, double vScreenPos)
        {
            if (v.Z + pp.Z < camera.Z) throw new Exception();

            double dVertexCamera = pp.Z + v.Z - camera.Z;
            double P = vScreenPos / dVertexCamera;


            if (dVertexCamera == 0) throw new Exception();

            int x = (int)((pp.X + v.X - camera.X) * P);
            int y = (int)((pp.Y + v.Y - camera.Y) * P);
            int z = (int)vScreenPos;


            return new Vertex(x - pp.X, y - pp.Y, z);
            //return new Vertex(v.X, v.Y, v.Z);
        }

    }
}
