﻿using Relative.DataStructures.Model;
using System;

namespace Relative.Core.Manager.ResourceClasses.Model
{
    public class Transformation
    {
        /// <summary>
        /// Method used to transform figure
        /// </summary>
        /// <param name="f">Figure to transform</param>
        /// <returns>Transformed figure</returns>
        public static Figure TransformateFigure(Figure f)
        {
            Figure res = f.Clone();

            for (int i = 0; i < f.Faces.Count; i++)
            {
                res.Faces[i] = TransformateFace(res.Faces[i], res.GetRotationRadians(), res.GetScale());
            }

            return res;
        }

        /// <summary>
        /// Method used to transform face
        /// </summary>
        /// <param name="f">Face to transform</param>
        /// <param name="rotation">Rotation value</param>
        /// <param name="scale">Scale value</param>
        /// <returns>Transformed face</returns>
        public static Face TransformateFace(Face f, double[] rotation, double scale)
        {
            f.TransformedSides = new Side[f.Sides.Length];

            for (int i = 0; i < f.Sides.Length; i++)
            {
                f.TransformedSides[i] = TransformateSide(f.Sides[i], rotation, scale);
            }

            return f;
        }

        /// <summary>
        /// Method used to transform side
        /// </summary>
        /// <param name="s">Transformed side</param>
        /// <param name="angles">Rotatation values foreach axis</param>
        /// <param name="scale">Scale value</param>
        /// <returns>Transformed side</returns>
        public static Side TransformateSide(Side s, double[] angles, double scale)
        {

            Vertex v1 = TransformateVertex(s.v1, angles, scale);
            Vertex v2 = TransformateVertex(s.v2, angles, scale);

            return new Side(v1, v2, s.Color);
        }

        /// <summary>
        /// Method used to transform Vertex
        /// </summary>
        /// <param name="v">Vertex to transform</param>
        /// <param name="angles">Rotation angles from all axis</param>
        /// <param name="scale">Scale to apply</param>
        /// <returns>Transformed vertex</returns>
        public static Vertex TransformateVertex(Vertex v, double[] angles, double scale)
        {
            Vertex transformed = v;

            transformed = Scalate(transformed, scale);

            transformed = Rotate(transformed, angles[0], VertexAxis.X_AXIS);
            transformed = Rotate(transformed, angles[1], VertexAxis.Y_AXIS);
            transformed = Rotate(transformed, angles[2], VertexAxis.Z_AXIS);

            return transformed;

        }

        /// <summary>
        /// Method used to scalate Vertex
        /// </summary>
        /// <param name="vertex">Vertex to scale</param>
        /// <param name="scale">Scale</param>
        /// <returns></returns>
        public static unsafe Vertex Scalate(Vertex vertex, double scale)
        {
            int x = (int)(vertex.X * scale),
                y = (int)(vertex.Y * scale),
                z = (int)(vertex.Z * scale);

            return new Vertex(x, y, z);

        }

        /// <summary>
        /// Method used to rotate vertex relatively to its PositionalPoint
        /// </summary>
        /// <param name="vertex">Vertex to rotate</param>
        /// <param name="angle">Angle</param>
        /// <param name="rotationType">Axis</param>
        /// <returns>Rotated vertex</returns>
        public static unsafe Vertex Rotate(Vertex vertex, double angle, VertexAxis rotationType)
        {
            int x = vertex.X,
                y = vertex.Y,
                z = vertex.Z;

            int* a, b;
            switch (rotationType)
            {
                case VertexAxis.X_AXIS:
                    a = &y;
                    b = &z;
                    break;
                case VertexAxis.Y_AXIS:
                    a = &x;
                    b = &z;
                    break;
                case VertexAxis.Z_AXIS:
                    a = &x;
                    b = &y;
                    break;
                default:
                    return new Vertex(x, y, z);
            }
            double[] polar = ToPolar(*a, *b);

            polar[1] += angle;

            *a = (int)(polar[0] * Math.Cos(polar[1]));
            *b = (int)(polar[0] * Math.Sin(polar[1]));

            return new Vertex(x, y, z);
        }

        /// <summary>
        /// Method used to transform verxex throught its polar coordinates (2 dimensions)
        /// </summary>
        /// <param name="a">First coordinate</param>
        /// <param name="b">Second coordinate</param>
        /// <returns>Transformed coordinates into polar coordinates</returns>
        public static double[] ToPolar(double a, double b)
        {
            double[] polar = new double[2];
            polar[0] = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            polar[1] = Math.Atan2(b, a);
            return polar;
        }

    }
}