﻿namespace Relative.Demo
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbEscala = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbY = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbZ = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnScale = new System.Windows.Forms.Button();
            this.btnPosX = new System.Windows.Forms.Button();
            this.btnPosY = new System.Windows.Forms.Button();
            this.btnPosZ = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbFOV = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnFOV = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(721, 407);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "fps";
            // 
            // tbEscala
            // 
            this.tbEscala.Location = new System.Drawing.Point(23, 45);
            this.tbEscala.Name = "tbEscala";
            this.tbEscala.Size = new System.Drawing.Size(100, 20);
            this.tbEscala.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Escala";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Posición (X)";
            // 
            // tbX
            // 
            this.tbX.Location = new System.Drawing.Point(23, 116);
            this.tbX.Name = "tbX";
            this.tbX.Size = new System.Drawing.Size(100, 20);
            this.tbX.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Posición (Y)";
            // 
            // tbY
            // 
            this.tbY.Location = new System.Drawing.Point(23, 180);
            this.tbY.Name = "tbY";
            this.tbY.Size = new System.Drawing.Size(100, 20);
            this.tbY.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Posición (Z)";
            // 
            // tbZ
            // 
            this.tbZ.Location = new System.Drawing.Point(23, 249);
            this.tbZ.Name = "tbZ";
            this.tbZ.Size = new System.Drawing.Size(100, 20);
            this.tbZ.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(169, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Auto";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(36, 14);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(75, 23);
            this.btnLeft.TabIndex = 11;
            this.btnLeft.Text = "←";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnLeft_MouseDown);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(300, 14);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(75, 23);
            this.btnRight.TabIndex = 12;
            this.btnRight.Text = "→";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnRight_MouseDown);
            // 
            // btnScale
            // 
            this.btnScale.Location = new System.Drawing.Point(129, 43);
            this.btnScale.Name = "btnScale";
            this.btnScale.Size = new System.Drawing.Size(45, 23);
            this.btnScale.TabIndex = 13;
            this.btnScale.Text = "Ok";
            this.btnScale.UseVisualStyleBackColor = true;
            this.btnScale.Click += new System.EventHandler(this.btnScale_Click);
            // 
            // btnPosX
            // 
            this.btnPosX.Location = new System.Drawing.Point(129, 113);
            this.btnPosX.Name = "btnPosX";
            this.btnPosX.Size = new System.Drawing.Size(45, 23);
            this.btnPosX.TabIndex = 14;
            this.btnPosX.Text = "Ok";
            this.btnPosX.UseVisualStyleBackColor = true;
            this.btnPosX.Click += new System.EventHandler(this.btnPosX_Click);
            // 
            // btnPosY
            // 
            this.btnPosY.Location = new System.Drawing.Point(129, 177);
            this.btnPosY.Name = "btnPosY";
            this.btnPosY.Size = new System.Drawing.Size(45, 23);
            this.btnPosY.TabIndex = 15;
            this.btnPosY.Text = "Ok";
            this.btnPosY.UseVisualStyleBackColor = true;
            this.btnPosY.Click += new System.EventHandler(this.btnPosY_Click);
            // 
            // btnPosZ
            // 
            this.btnPosZ.Location = new System.Drawing.Point(129, 246);
            this.btnPosZ.Name = "btnPosZ";
            this.btnPosZ.Size = new System.Drawing.Size(45, 23);
            this.btnPosZ.TabIndex = 16;
            this.btnPosZ.Text = "Ok";
            this.btnPosZ.UseVisualStyleBackColor = true;
            this.btnPosZ.Click += new System.EventHandler(this.btnPosZ_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnLeft);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnRight);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 347);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(721, 60);
            this.panel1.TabIndex = 17;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbFOV);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.btnFOV);
            this.panel2.Controls.Add(this.tbEscala);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnPosZ);
            this.panel2.Controls.Add(this.tbX);
            this.panel2.Controls.Add(this.btnPosY);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.btnPosX);
            this.panel2.Controls.Add(this.tbY);
            this.panel2.Controls.Add(this.btnScale);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.tbZ);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(521, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 347);
            this.panel2.TabIndex = 18;
            // 
            // tbFOV
            // 
            this.tbFOV.Location = new System.Drawing.Point(23, 311);
            this.tbFOV.Name = "tbFOV";
            this.tbFOV.Size = new System.Drawing.Size(100, 20);
            this.tbFOV.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 292);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "FOV Degrees";
            // 
            // btnFOV
            // 
            this.btnFOV.Location = new System.Drawing.Point(129, 309);
            this.btnFOV.Name = "btnFOV";
            this.btnFOV.Size = new System.Drawing.Size(45, 23);
            this.btnFOV.TabIndex = 19;
            this.btnFOV.Text = "Ok";
            this.btnFOV.UseVisualStyleBackColor = true;
            this.btnFOV.Click += new System.EventHandler(this.btnFOV_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 407);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Exit);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbEscala;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbZ;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnScale;
        private System.Windows.Forms.Button btnPosX;
        private System.Windows.Forms.Button btnPosY;
        private System.Windows.Forms.Button btnPosZ;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbFOV;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnFOV;
    }
}

