﻿using Relative.Core.Manager.ResourceClasses.Model;
using Relative.DataStructures.Model;
using Relative.Presentation.Module.Interpreter.AccesClasses;
using Relative.Presentation.Module.Persistence.AccesClasses;
using Relative.Presentation.Module.Transformer.AccesClasses;
using System;
using System.Threading;
using System.Windows.Forms;

namespace Relative.Demo
{
    public partial class Form1 : Form
    {

        Transformer transformer;
        Interpreter interpreter;

        Vertex camera;

        int speed, speedRotation;

        bool auto = false;
        Thread animation;

        public Form1()
        {
            speed = 20;
            speedRotation = 15;

            InitializeComponent();

            transformer = Transformer.GetInstance(@"..\..\..\Relative.Demo\scenarios\scenario1.xml");
            interpreter = Interpreter.GetInstance(pictureBox1, label1);
            interpreter.Start();

            camera = transformer.GetCamera();

            animation = new Thread(Animation);

            tbEscala.Text = "" + transformer.GetFigures()[0].GetScale();
            tbX.Text = "" + transformer.GetFigures()[0].PositionalPoint.X;
            tbY.Text = "" + transformer.GetFigures()[0].PositionalPoint.Y;
            tbZ.Text = "" + transformer.GetFigures()[0].PositionalPoint.Z;
            tbFOV.Text = "" + transformer.GetFovDegrees();

        }

        public void Animation()
        {
            Thread.Sleep(100);
            double rotation = transformer.GetFigures()[0].GetRotationDegrees(VertexAxis.Y_AXIS);

            do
            {
                rotation += 1;
                  
                transformer.GetFigures()[0].SetRotationDegrees(VertexAxis.Y_AXIS, rotation);

                Thread.Sleep(15);

            } while (true);
        }

        private void Exit(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (auto)
            {
                animation.Suspend();
                animation = new Thread(Animation);

                button1.Text = "Auto";
                btnLeft.Enabled = true;
                btnRight.Enabled = true;
                auto = false;
            }
            else
            {
                animation.Start();

                button1.Text = "Stop";
                btnLeft.Enabled = false;
                btnRight.Enabled = false;
                auto = true;
            }

        }

        private void btnRight_MouseDown(object sender, MouseEventArgs e)
        {
            double rotation = transformer.GetFigures()[0].GetRotationDegrees(VertexAxis.Y_AXIS) + speedRotation;
            transformer.GetFigures()[0].SetRotationDegrees(VertexAxis.Y_AXIS, rotation);
        }

        private void btnLeft_MouseDown(object sender, MouseEventArgs e)
        {
            double rotation = transformer.GetFigures()[0].GetRotationDegrees(VertexAxis.Y_AXIS) - speedRotation;
            transformer.GetFigures()[0].SetRotationDegrees(VertexAxis.Y_AXIS, rotation);
        }

        private void btnScale_Click(object sender, EventArgs e)
        {
            double escala = Double.Parse(tbEscala.Text);
            transformer.GetFigures()[0].SetScale(escala);
        }

        private void btnPosX_Click(object sender, EventArgs e)
        {
            int pos = Int32.Parse(tbX.Text);
            transformer.GetFigures()[0].PositionalPoint.X = pos;
        }

        private void btnPosY_Click(object sender, EventArgs e)
        {
            int pos = Int32.Parse(tbY.Text);
            transformer.GetFigures()[0].PositionalPoint.Y = pos;
        }

        private void btnFOV_Click(object sender, EventArgs e)
        {
            double fov = Int32.Parse(tbFOV.Text);
            transformer.SetFovDegrees(fov);
        }

        private void btnPosZ_Click(object sender, EventArgs e)
        {
            int pos = Int32.Parse(tbZ.Text);
            transformer.GetFigures()[0].PositionalPoint.Z = pos;
        }
        

    }
}
