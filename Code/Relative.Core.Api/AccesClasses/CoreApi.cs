﻿using Relative.Core.Manager;
using Relative.Core.Manager.ResourceClasses.Model;

namespace Relative.Core.Api
{
    public class CoreApi
    {
        #region Attributes

        private static CoreApi instance = null;
        private CoreManager manager;

        #endregion Attributes

        #region Constructors

        private CoreApi(int w, int h)
        {
            manager = CoreManager.GetInstance(w, h);
        }

        private CoreApi(Scenario s)
        {
            manager = CoreManager.GetInstance(s);
        }

        /// <summary>
        /// Singleton Constructor for interpreter
        /// </summary>
        /// <param name="w">VirtualScreen width</param>
        /// <param name="h">VirtualScreen Height</param>
        /// <returns>Api acces</returns>
        public static CoreApi GetInstance(int w, int h)
        {
            if (instance == null)
            {
                instance = new CoreApi(w, h);
            }
            else
            {
                instance.manager = CoreManager.GetInstance(w, h);
            }

            return instance;
        }

        /// <summary>
        /// Singleton Constructor for transformer
        /// </summary>
        /// <param name="s">Collection of figures</param>
        /// <returns>Api acces</returns>
        public static CoreApi GetInstance(Scenario s)
        {
            if (instance == null)
            {
                instance = new CoreApi(s);
            }

            return instance;
        }

        #endregion Constructors

        #region Getters and Setters        

        public int[,] GetMatrix()
        {
            return manager.GetMatrix();
        }

        public Scenario GetScenario()
        {
            return manager.GetScenario();
        }

        #endregion Getters and Setters    

        #region Methods

        /// <summary>
        /// Start Matrix through manager
        /// </summary>
        public void GenerateMatrix()
        {
            manager.Run();
        }

        #endregion Methods

    }
}
