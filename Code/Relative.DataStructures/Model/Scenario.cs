﻿using System;
using System.Collections.Generic;

namespace Relative.Core.Manager.ResourceClasses.Model
{
    public class Scenario
    {

        #region Constructors

        public Scenario(List<Figure> Figures)
        {
            this.Fov = Math.PI / 3 * 2;
            this.Figures = Figures;
        }
        public Scenario() : this(new List<Figure>()) {}

        #endregion Constructors

        #region Getters and Setters

        public List<Figure> Figures { get; set; }
        public double Fov { get; set; }
        public Vertex Camera { get; set; }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Method used to clone scenario
        /// </summary>
        /// <returns></returns>
        public Scenario Clone()
        {
            Scenario cloned = new Scenario();

            cloned.Camera = Camera.Clone();
            cloned.Fov = Fov;

            for (int i = 0; i < Figures.Count; i++)
            {
                cloned.Figures.Add(Figures[i].Clone());
            }

            return cloned;
        }

        #endregion Methods

    }
}
