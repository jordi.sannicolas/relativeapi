﻿
namespace Relative.DataStructures.Model
{
    /// <summary>
    /// The vertices of each of the dimensions are represented.
    /// </summary>
    public enum VertexAxis
    {

        X_AXIS = 0, Y_AXIS = 1, Z_AXIS = 2

    }
}
