﻿using Relative.DataStructures.Model;
using System;
using System.Collections.Generic;

namespace Relative.Core.Manager.ResourceClasses.Model
{
    public class Figure
    {
        #region Attributes

        private double[] rotation = new double[3];
        private double scale;

        #endregion Attributes

        #region Constructor

        public Figure(Vertex positionPoint, List<Face> faces)
        {
            Faces = faces;
            PositionalPoint = positionPoint;
            scale = 1;
        }

        #endregion Constructor

        #region Getters and Setters

        public List<Face> Faces { get; set; }
        public Vertex PositionalPoint { get; set; }

        public double[] GetRotationRadians()
        {
            return rotation;
        }
        public double GetRotationRadians(VertexAxis axis)
        {
            return rotation[(int)axis];
        }

        public void SetRotationRadians(VertexAxis axis, double angle)
        {
            rotation[(int)axis] = angle;
        }

        public double GetRotationDegrees(VertexAxis axis)
        {
            return rotation[(int)axis] * 180 / Math.PI;
        }

        public void SetRotationDegrees(VertexAxis axis, double angle)
        {
            rotation[(int)axis] = angle * Math.PI / 180;
        }

        public double GetScale()
        {
            return scale;
        }

        public void SetScale(double scale)
        {
            this.scale = scale;
        }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Method used to clone figure
        /// </summary>
        /// <returns>New instance with cloned figure</returns>
        public Figure Clone()
        {
            List<Face> faces = new List<Face>();
            foreach (Face f in Faces) faces.Add(f.Clone());
            Figure cloned = new Figure(PositionalPoint.Clone(), faces);

            cloned.rotation = rotation;
            cloned.scale = scale;

            return cloned;
        }

        #endregion Methods

    }
}
