﻿

namespace Relative.Core.Manager.ResourceClasses.Model
{
    public class Face
    {

        #region Constructor

        public Face(Side[] sides)
        {
            Sides = sides;
            TransformedSides = sides;
        }

        #endregion Constructor

        #region Getters and Setters

        public Side[] Sides { get; set; }
        public Side[] TransformedSides { get; set; }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Method used to clone face
        /// </summary>
        /// <returns>New instance with face</returns>
        public Face Clone()
        {
            Face cloned = new Face(new Side[Sides.Length]);

            for (int i = 0; i < Sides.Length; i++)
            {
                cloned.Sides[i] = Sides[i].Clone();
            }

            for (int i = 0; i < TransformedSides.Length; i++)
            {
                cloned.TransformedSides[i] = TransformedSides[i]?.Clone();
            }

            return cloned;
        }

        #endregion Methods

    }
}
