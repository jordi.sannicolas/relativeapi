﻿

namespace Relative.Core.Manager.ResourceClasses.Model
{
    public class Vertex
    {

        #region Constructor

        public Vertex(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        #endregion Constructor

        #region Getters and Setters

        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Method used to clone vertex
        /// </summary>
        /// <returns>New instance with cloned vertex</returns>
        public Vertex Clone()
        {
            return new Vertex(X, Y, Z);
        }

        #endregion Methods

    }
}
