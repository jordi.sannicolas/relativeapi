﻿
namespace Relative.Core.Manager.ResourceClasses.Model
{
    public class Side
    {

        #region Attributes

        private Vertex _v1, _v2;
        public float multiplicator, addingUp;

        #endregion Attributes

        #region Constructor

        public Side(Vertex v1, Vertex v2, int color)
        {
            _v1 = v1;
            _v2 = v2;
            Color = color;

            ResolveFunction();
        }

        #endregion Constructor

        #region Getters and Setters

        public int Color { get; set; }
        public Vertex v1
        {
            get
            {
                return _v1;
            }
            set
            {
                _v1 = value;
                ResolveFunction();
            }
        }
        public Vertex v2
        {
            get
            {
                return _v2;
            }
            set
            {
                _v2 = value;
                ResolveFunction();
            }
        }

        public void SetVertices(Vertex v1, Vertex v2)
        {
            _v1 = v1;
            _v2 = v2;
            ResolveFunction();
        }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Method used to resolve the straight function (its draw)
        /// </summary>
        private void ResolveFunction()
        {
            float dy = v1.Y - v2.Y;
            float dx = v1.X - v2.X;

            if (dx == 0) multiplicator = float.PositiveInfinity;
            else multiplicator = dy / dx;

            addingUp = v1.Y - multiplicator * v1.X;

        }

        /// <summary>
        /// Method used to clone side
        /// </summary>
        /// <returns>New instance with cloned side</returns>
        public Side Clone()
        {
            return new Side(v1.Clone(), v2.Clone(), Color);
        }

        #endregion Methods

    }
}
