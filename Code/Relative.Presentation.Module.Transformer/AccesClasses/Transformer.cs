﻿using Relative.Core.Api;
using Relative.Core.Manager.ResourceClasses.Model;
using Relative.Presentation.Module.Persistence.AccesClasses;
using System;
using System.Collections.Generic;

namespace Relative.Presentation.Module.Transformer.AccesClasses
{
    public class Transformer
    {
        #region Attributes

        private static Transformer instance = null;
        private CoreApi api;

        #endregion Attributes

        #region Constructors

        private Transformer(string data, bool isPath = true)
        {

            ScenarioReader r;

            if (isPath)
            {
                r = new ScenarioReader(data);
            }
            else
            {
                r = new ScenarioReader(data, false);
            }

            api = CoreApi.GetInstance(r.GetScenario());


        }

        /// <summary>
        /// Singleton Constructor
        /// </summary>
        /// <param name="scenario">Scenario to which figures belong</param>
        /// <returns></returns>
        public static Transformer GetInstance(string data, bool isPath = true)
        {
            if (instance == null)
            {
                instance = new Transformer(data, isPath);
            }

            return instance;

        }

        #endregion Constructors

        #region Getters and Setters

        public void SetCamera(Vertex v)
        {
            api.GetScenario().Camera = v;
        }

        public Vertex GetCamera()
        {
            return api.GetScenario().Camera;
        }


        public List<Figure> GetFigures()
        {
            return api.GetScenario().Figures;
        }

        public double GetFovRadians()
        {
            return api.GetScenario().Fov;
        }

        public void SetFovRadians(double fov)
        {
            api.GetScenario().Fov = fov;
        }

        public double GetFovDegrees()
        {
            return api.GetScenario().Fov * 180 / Math.PI;
        }

        public void SetFovDegrees(double fov)
        {
            api.GetScenario().Fov = fov / 180 * Math.PI;
        }

        #endregion Getters and Setters

    }
}
