﻿using Relative.Core.Manager.ResourceClasses.Model;
using Relative.DataStructures.Model;
using Relative.Presentation.Module.Interpreter.AccesClasses;
using Relative.Presentation.Module.Persistence.AccesClasses;
using Relative.Presentation.Module.Transformer.AccesClasses;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Relative.DemoServlet
{
    public partial class Form1 : Form
    {

        Transformer transformer;
        Interpreter interpreter;
        string URL = "http://192.168.127.132:8080/relative-servlet/scenario?action=scenarioByName&name=scenario10";

        public Form1()
        {
            InitializeComponent();            
            
            transformer = Transformer.GetInstance(GetHttp(), false);
            interpreter = Interpreter.GetInstance(pictureBox1, label1);            
            interpreter.Start();

            new Thread(Animation).Start();

        }

        public string GetHttp()
        {
            WebRequest wreq = WebRequest.Create(URL);
            WebResponse wres = wreq.GetResponse();
            StreamReader sr = new StreamReader(wres.GetResponseStream());
            return sr.ReadToEnd();
        }

        public void Animation()
        {
            Thread.Sleep(100);
            double rotation = 0;

            do
            {
                rotation += Math.PI / 100;

                lock (transformer.GetFigures())
                {
                    transformer.SetCamera(new Vertex(
                        (int)(300 * Math.Sin(rotation)) - 100,
                        (int)(100 * Math.Cos(rotation * 2)) + 20,
                        (int)(200 * Math.Sin(rotation))

                    ));
                    transformer.GetFigures()[0].SetRotationRadians(VertexAxis.Y_AXIS, rotation);
                }

                Thread.Sleep(15);

            } while (true);
        }

        private void Exit(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(1);
        }
    }
}
