﻿using Relative.Core.Manager.ResourceClasses.Model;
using Relative.Presentation.Module.Persistence.ResourceClasses;
using System.IO;
using System.Text;

namespace Relative.Presentation.Module.Persistence.AccesClasses
{
    public class ScenarioReader
    {

        #region Attributes

        private ReadXml readXml;

        #endregion Attributes

        #region Constructors
        public ScenarioReader(string data, bool isPath = true)
        {
            if (isPath)
            {
                Path = data;
                readXml = new ReadXml(data);
            }
            else
            {
                MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(data));
                readXml = new ReadXml(stream);
            }
        }

        public ScenarioReader(Stream stream)
        {
            readXml = new ReadXml(stream);
        }
        #endregion Constructors

        #region Getters and Setters

        public string Path { get; set; }

        #endregion Getters and Setters

        #region Methods

        /// <summary>
        /// Request scenario from ReadXml
        /// </summary>
        /// <returns>Return the scenario with the figures</returns>
        public Scenario GetScenario()
        {
            return readXml.GetScenario();
        }

        #endregion Methods

    }
}
