﻿using Relative.Core.Manager.ResourceClasses.Model;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace Relative.Presentation.Module.Persistence.ResourceClasses
{
    class ReadXml
    {

        #region Attributes

        private Scenario scenario;
        private Figure figure;
        private Face face;
        private Side side;
        private Vertex vertex1, vertex2;
        private List<Side> sides;
        private int color;
        private Vertex positionalPoint, camera;
        private string tag;

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Constructor that reads xml
        /// </summary>
        /// <param name="path">xml path</param>
        public ReadXml(string path)
        {
            sides = new List<Side>();
            Stream stream = new FileStream(path, FileMode.Open);
            Read(stream);
        }

        public ReadXml(Stream stream)
        {
            sides = new List<Side>();
            Read(stream);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Reads the xml nodes
        /// </summary>
        private void Read(Stream stream)
        {

            XmlReader xmlReader = XmlReader.Create(stream);

            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        OpenNode(xmlReader);
                        break;

                    case XmlNodeType.Text:
                        ReadNode(xmlReader);
                        break;

                    case XmlNodeType.EndElement:
                        CloseNode(xmlReader);
                        break;
                }
            }

        }

        /// <summary>
        /// The necessary attributes are instantiated when a node is opened.
        /// </summary>
        /// <param name="xmlReader">XmlReader that has all data read</param>
        private void OpenNode(XmlReader xmlReader)
        {
            tag = xmlReader.Name;
            switch (xmlReader.Name)
            {
                case "scenario":
                    scenario = new Scenario(new List<Figure>());
                    break;
                case "figure":
                    figure = new Figure(new Vertex(0, 0, 0), new List<Face>());
                    break;
                case "face":
                    sides.Clear();
                    face = new Face(new Side[0]);
                    break;
                case "vertex":
                    AssignDataToVertex(xmlReader);
                    break;
                case "positionalpoint":
                    positionalPoint = new Vertex(
                        int.Parse(xmlReader.GetAttribute("x")),
                        int.Parse(xmlReader.GetAttribute("y")),
                        int.Parse(xmlReader.GetAttribute("z"))
                    );
                    break;
                case "camera":
                    camera = new Vertex(
                        int.Parse(xmlReader.GetAttribute("x")),
                        int.Parse(xmlReader.GetAttribute("y")),
                        int.Parse(xmlReader.GetAttribute("z"))
                    );
                    break;
            }

        }

        /// <summary>
        /// Reads the vertex tag and allocates the data
        /// </summary>
        /// <param name="xmlReader">XmlReader that has all data read</param>
        private void AssignDataToVertex(XmlReader xmlReader)
        {
            Vertex vertex = new Vertex(
                                    int.Parse(xmlReader.GetAttribute("x")),
                                    int.Parse(xmlReader.GetAttribute("y")),
                                    int.Parse(xmlReader.GetAttribute("z"))
                                );

            if (vertex1 == null)
            {
                vertex1 = vertex;
            }
            else
            {
                vertex2 = vertex;
            }
        }

        /// <summary>
        /// Stores data according to the node's text
        /// </summary>
        /// <param name="xmlReader">XmlReader that has all data read</param>
        private void ReadNode(XmlReader xmlReader)
        {

            switch (tag)
            {
                case "color":
                    color = ReadColor(xmlReader.Value);
                    break;
            }

        }

        /// <summary>
        /// Adds necessary attributes to lists when nodes are closed
        /// </summary>
        /// <param name="xmlReader">XmlReader that has all data read</param>
        private void CloseNode(XmlReader xmlReader)
        {
            switch (xmlReader.Name)
            {
                case "scenario":
                    scenario.Camera = camera;
                    break;
                case "figure":
                    scenario.Figures.Add(figure);
                    figure.PositionalPoint = positionalPoint;
                    break;
                case "face":
                    face.Sides = sides.ToArray();
                    figure.Faces.Add(face);
                    break;
                case "side":
                    side = new Side(vertex1, vertex2, color);
                    vertex1 = null;
                    sides.Add(side);
                    break;
            }
        }

        /// <summary>
        /// Coloured text that is converted to integer
        /// </summary>
        /// <param name="color">Colour that parse to int</param>
        /// <returns>returns the colour converted to integer</returns>
        private int ReadColor(string color)
        {
            return int.Parse(color.Substring(2), System.Globalization.NumberStyles.HexNumber);
        }

        /// <summary>
        /// Getter of scenario
        /// </summary>
        /// <returns>returns scenario</returns>
        public Scenario GetScenario()
        {
            return scenario;
        }

        #endregion Methods

    }
}
